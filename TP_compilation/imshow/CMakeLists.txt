cmake_minimum_required (VERSION 3.0)
project(imshow)
find_package(PkgConfig REQUIRED)

pkg_check_modules(CV REQUIRED opencv)
include_directories(${CV_INCLUDE_DIRECTORIES})

add_executable(imshow imshow.cpp)
target_link_libraries(imshow ${CV_LIBRARIES})


