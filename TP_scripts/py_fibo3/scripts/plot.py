
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import fibo
import sinus

xs = range(10)
ys = [fibo.fiboIterative(x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('plot_fibo.png')
plt.clf()

xs = [x/100.0 for x in range(100)]
ys = [sinus.sinus(2.0,0.25,x) for x in xs]
plt.plot(xs,ys)
plt.xlabel('x')
plt.ylabel('sin(x)')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()
