#include "Fibo.hpp"
#include <string>

int fibo(int n, int f0, int f1) {
    if(f0>f1) throw std::string("fibo decroissante");
    if(f0<0) throw std::string("fibo negative");
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

