#include "Spline.hpp"
#include <iostream>
#include <string>
#include <glog/logging.h>
using namespace std;

int main() {

    // begin
    google::InitGoogleLogging("log");
    google::SetLogDestination(google::GLOG_INFO, "log");
    // create spline
    Spline spline;

    // add keys
    double t;
    Vec2 P;
    try{
        LOG(INFO) << "Récupération des valeurs ...";
        while (cin >> t and cin >> P.x_ and cin >> P.y_) {
            spline.addKey(t, P);
        }
        LOG(INFO) << "Terminé.\n";
        LOG(INFO) << "Calcul des données ...";

        // compute values
        double deltaT = (spline.getEndTime() - spline.getStartTime()) / 100.0;
        for (double t=spline.getStartTime(); t<spline.getEndTime(); t+=deltaT) {
            Vec2 Pt = spline.getValue(t);
            cout << t << " " << Pt.x_ << " " << Pt.y_ << "\n";
        }
        LOG(INFO) << "Terminé.\n";
    }
    catch(string e){
        //cerr << e << endl;
        LOG(ERROR) << e;
        return -1;
    }

    // end

    return 0;
}

